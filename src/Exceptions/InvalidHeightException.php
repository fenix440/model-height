<?php  namespace Fenix440\Model\Height\Exceptions; 
/**
 * Class InvalidHeightException
 *
 * Throws an exception when height is invalid
 *
 * @package Fenix440\Model\Height\Exceptions 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class InvalidHeightException extends \InvalidArgumentException{

 

}

 