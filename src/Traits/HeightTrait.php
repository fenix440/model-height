<?php namespace Fenix440\Model\Height\Traits;
use Aedart\Validate\Number\FloatValidator;
use Fenix440\Model\Height\Exceptions\InvalidHeightException;

/**
 * Trait HeightTrait
 *
 * @see HeightAware
 *
 * @package      Fenix440\Model\Height\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 */
trait HeightTrait
{

    /**
     * Height for given component
     *
     * @var null|float
     */
    protected $height = null;

    /**
     * Set height for given component
     *
     * @param float $height Height for given component
     * @return void
     * @throws InvalidHeightException If height is invalid
     */
    public function setHeight($height){
        if(!$this->isHeightValid((float)$height))
            throw new InvalidHeightException(sprintf('Height "%d" is invalid',$height));
        $this->height=$height;
    }

    /**
     * Get height
     *
     * @return float|null
     */
    public function getHeight(){
        if(!$this->hasHeight() && $this->hasDefaultHeight())
            $this->setHeight($this->getDefaultHeight());
        return $this->height;
    }

    /**
     * Get default height
     *
     * @return float|null
     */
    public function getDefaultHeight(){
        return null;
    }

    /**
     * Validates if height is valid
     *
     * @param float $height Height for given component
     * @return bool true/false
     */
    public function isHeightValid($height){
        return FloatValidator::isValid($height,[FloatValidator::MAX_PRECISION_RANGE => 2, FloatValidator::INCLUSIVE_PRECISION_RANGE=> true]) && $height > 0;
    }

    /**
     * Checks if height is set
     *
     * @return bool true/false
     */
    public function hasHeight(){
        return (!is_null($this->height))? true:false;
    }

    /**
     * Checks if default height is set
     *
     * @return bool true/false
     */
    public function hasDefaultHeight(){
        return (!is_null($this->getDefaultHeight()))? true:false;
    }



}