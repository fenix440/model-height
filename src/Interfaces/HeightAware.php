<?php namespace Fenix440\Model\Height\Interfaces;
use Fenix440\Model\Height\Exceptions\InvalidHeightException;

/**
 * Interface HeightAware
 *
 * A component must be aware of height property
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Height\Interfaces
 */
interface HeightAware {

    /**
     * Set height for given component
     *
     * @param float $height Height for given component
     * @return void
     * @throws InvalidHeightException If height is invalid
     */
    public function setHeight($height);

    /**
     * Get height
     *
     * @return float|null
     */
    public function getHeight();

    /**
     * Get default height
     *
     * @return float|null
     */
    public function getDefaultHeight();

    /**
     * Validates if height is valid
     *
     * @param float $height Height for given component
     * @return bool true/false
     */
    public function isHeightValid($height);

    /**
     * Checks if height is set
     *
     * @return bool true/false
     */
    public function hasHeight();

    /**
     * Checks if default height is set
     *
     * @return bool true/false
     */
    public function hasDefaultHeight();

}