## Model-Height ##

Getter and Setter packheight for some kind of model height.

This packheight is part of a project `Aedart\Model`, visit [https://bitbucket.org/aedart/model](https://bitbucket.org/aedart/model) to learn more about it.

Official sub-packheight website ([https://bitbucket.org/fenix440/model-height](https://bitbucket.org/fenix440/model-height))

## Contents ##

[TOC]

## When to use this ##

When your component(s) need to be aware of height property

## How to install ##

```
#!console

composer require fenix440/model-height  versionNumber
```

This packheight uses [composer](https://getcomposer.org/). If you do not know what that is or how it works, I recommend that you read a little about, before attempting to use this packheight.

## Quick start ##


Provided that you have an interface, e.g. for a product, you can extend the weight aware interface;

```
#!php
<?php
use Fenix440\Model\Height\Interfaces\HeightAware;

interface IPerson extends HeightAware{

    // ... Remaining interface implementation not shown

}
```

In your class implementation, you simple use one of the name traits.

```
#!php
<?php
use Fenix440\Model\Height\Traits\HeightTrait;

class MyPerson implements IPerson {
   
     use HeightTrait;
   
     // ... Remaining implementation not shown...

}
```

## License ##

[BSD-3-Clause](http://spdx.org/licenses/BSD-3-Clause), Read the LICENSE file included in this packheight