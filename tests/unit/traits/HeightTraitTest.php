<?php
use Fenix440\Model\Height\Traits\HeightTrait;
use Fenix440\Model\Height\Interfaces\HeightAware;

/**
 * Class HeightTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Height\Traits\HeightTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class HeightTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Height\Interfaces\HeightAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Height\Traits\HeightTrait');
    }


    /**
     * Get dummy class
     * @return DummyClass
     */
    protected function getDummyClass(){
        return new DummyClass();
    }
    /************************************************************************
     * Actual tests
     ***********************************************************************/


    /**
     * @test
     * @covers  ::setHeight
     * @covers  ::isHeightValid
     * @covers  ::getHeight
     * @covers  ::getDefaultHeight
     * @covers  ::hasDefaultHeight
     * @covers  ::hasHeight
     *
     */
    public function setAndGetHeight()
    {
        $trait = $this->getTraitMock();
        $height = (float)182;
        $trait->setHeight($height);

        $this->assertSame($height,$trait->getHeight(),'Invalid set and get height');

    }

    /**
     * @test
     * @covers  ::setHeight
     * @covers  ::isHeightValid
     * @expectedException \Fenix440\Model\Height\Exceptions\InvalidHeightException
     */
    public function setInvalidFloatHeight()
    {
        $trait = $this->getTraitMock();
        $height=23.232;

        $trait->setHeight($height);
    }

    /**
     * @test
     * @covers  ::setHeight
     * @covers  ::isHeightValid
     * @expectedException \Fenix440\Model\Height\Exceptions\InvalidHeightException
     */
    public function setInvalidIntegerHeight()
    {
        $trait = $this->getTraitMock();
        $height=0;

        $trait->setHeight($height);
    }

    /**
     * @test
     * @covers ::getDefaultHeight
     */
    public function testDefaultHeight()
    {
        $trait = $this->getTraitMock();

        $this->assertNull($trait->getDefaultHeight(),'Default height is invalid');
    }

    public function testHasDefaultHeight()
    {
        $trait = $this->getTraitMock();

        $this->assertFalse($trait->hasDefaultHeight(),'Default height is set!');
    }

    /**
     * @test
     * @covers ::setHeight
     * @covers ::isHeightValid
     * @covers ::getHeight
     * @covers ::getDefaultHeight
     * @covers ::hasDefaultHeight
     * @covers ::hasHeight
     */
    public function testSetDefaultHeight()
    {
        $dumy = $this->getDummyClass();

        $this->assertSame(DummyClass::DEFAULT_HEIGHT, $dumy->getHeight(), 'Invalid default height');
    }

}

class DummyClass{

    use HeightTrait;

    const DEFAULT_HEIGHT = 200.00;

    /**
     * Get default height
     *
     * @return float|int|null
     */
    public function getDefaultHeight(){
        return self::DEFAULT_HEIGHT;
    }
}